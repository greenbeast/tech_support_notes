import sys, os
import pandas as pd
import sqlite3
import argparse

conn = sqlite3.connect("tech_support_notes.db")
c = conn.cursor()

"""
This makes it able to take a df and 
put it into a sqlite database
https://stackoverflow.com/questions/2887878/importing-a-csv-file-into-a-sqlite3-database-table-using-python
"""

"""
documentation for df.to_sql:
https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_sql.html?highlight=to_sql

documentation for df.read_sql:
https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_sql.html#pandas.read_sql
"""


# path = "/home/hank/Development/tech_support/tech_support_notes/tech_support_notes.csv"

path = os.path.join(sys.path[0], 'tech_support_notes.csv')

df = pd.read_csv(path)

df.drop(["Unnamed: 18", "Unnamed: 19"], axis=1, inplace=True)

sorted_df = df.sort_values("Created_timestamp", ascending=False)

sorted_df.columns = [
    "Name",
    "Column",
    "Swimlane",
    "Time_estimate",
    "Description",
    "Color",
    "Responsible",
    "Members",
    "Time_spent",
    "Labels",
    "Due_dates",
    "Subtasks",
    "Comments",
    "File_attachments",
    "Relations",
    "Grouping_date",
    "Created_timestamp",
    "Task_URL",
]


def create_db():
    sql = """CREATE TABLE Notes 
             (Name, Column, Swimlane, Time_estimate, Description, Color, 
              Responsible, Members, Time_spent, Labels, Due_dates, Subtasks, 
              Comments, File_attachments, Relations, Grouping_date, Created_timestamp, Task_URL)"""
    c.execute(sql)
    conn.commit()
    print("Data done been committed, ya hear?")


def write_db():
    sorted_df.to_sql("Notes", conn, if_exists="append", index=False)
    print("You done rit to the database")


def read_db():
    sql = "select * from Notes"
    sorted_db = pd.read_sql(sql, conn)
    print("reading database")
    #print(sorted_db)


def db_to_csv():
    csv_path = '/home/hank/Development/tech_support/tech_support_notes/'
    sql = "select * from Notes"
    df = pd.read_sql(sql, conn)
    df.to_csv(csv, index=False)
    #print(f"CSV has printed to {csv_db}")


try:
    parser = argparse.ArgumentParser()
    parse.add_argument('--db', dest='Database', choices=['read','write','create','csv'],
                       help='Either read, write or create a db or print it out to a spreadsheet.')
    args = parse.parse_args()
    fmt = args.Database
    if fmt == 'read':
        read_db()
    elif fmt == 'write':
        write_db()
    elif fmt == 'create':
        create_db()
    elif fmt == 'csv':
        db_to_csv()

except Exception as e:
    print(e)
